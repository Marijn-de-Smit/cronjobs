package com.Winnable.CronJobs.domain;

import enums.Category;

import javax.persistence.*;

@Entity
public class Lottery {

    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Id
    private int id;

    private String name;
    private Category category;

    public Lottery() {
    }

    public Lottery(int id) {
        this.id = id;
    }

    public Lottery(String name, Category category) {
        this.name = name;
        this.category = category;
    }

    public Lottery(int id, String name, Category category) {
        this.id = id;
        this.name = name;
        this.category = category;
    }

    public int getId() {return this.id;}

    public String getName() {
        return name;
    }
    public void setName(String name) {
        this.name = name;
    }

    public Category getCategory() {
        return category;
    }
    public void setCategory(Category category) {
        this.category = category;
    }

}
