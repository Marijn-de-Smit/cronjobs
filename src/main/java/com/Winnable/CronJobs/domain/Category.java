package com.Winnable.CronJobs.domain;

import javax.persistence.*;

@Entity
public class Category {

    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Id
    private int id;

    private String name;

    public Category() {
    }

    public Category(int id) {
        this.id = id;
    }

    public Category(String name) {
        this.name = name;
    }

    public Category(int id, String name) {
        this.id = id;
        this.name = name;
    }

    public int getId() {return this.id;}

    public String getName() {
        return name;
    }
    public void setName(String name) {
        this.name = name;
    }
}
