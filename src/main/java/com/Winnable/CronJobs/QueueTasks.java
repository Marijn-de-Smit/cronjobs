package com.Winnable.CronJobs;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Component;

@Component
public class QueueTasks {
    static final Logger logger = LoggerFactory.getLogger(QueueTasks.class);

    @RabbitListener(queues = RabbitConfig.QUEUE)
    public void processMessage(){
        logger.info("Message received!");
    }
}
