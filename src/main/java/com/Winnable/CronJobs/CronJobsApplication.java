package com.Winnable.CronJobs;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.config.server.EnableConfigServer;
import org.springframework.scheduling.annotation.EnableScheduling;

@EnableScheduling
@EnableConfigServer
@SpringBootApplication
public class CronJobsApplication {

    public static void main(String[] args) {
        SpringApplication.run(CronJobsApplication.class, args);
    }
}
